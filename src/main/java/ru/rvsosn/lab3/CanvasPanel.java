package ru.rvsosn.lab3;

import javax.swing.*;
import javax.swing.event.MouseInputListener;
import java.awt.*;
import java.awt.event.MouseEvent;

public class CanvasPanel extends JPanel {
    private final MyPeerServer server;
    private Point clicked = null;

    public CanvasPanel(MyPeerServer server) {
        this.server = server;
        this.server.setOnNewLine((line) -> repaint());

        this.addMouseListener(new MouseInputListener() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                int x = mouseEvent.getX();
                int y = mouseEvent.getY();

                Point point = new Point(x, y);

                if (clicked == null) {
                    clicked = point;
                } else {
                    server.newLine(new Line(clicked, point));
                    clicked = null;
                    repaint();
                }
            }

            @Override
            public void mousePressed(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseReleased(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseEntered(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseExited(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseDragged(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseMoved(MouseEvent mouseEvent) {

            }
        });
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        for (Line line : server.getLines()) {
            g.drawLine(line.getFrom().getX(), line.getFrom().getY(), line.getTo().getX(), line.getTo().getY());
        }
    }
}

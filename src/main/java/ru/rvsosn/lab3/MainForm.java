package ru.rvsosn.lab3;

import javax.swing.*;
import java.util.Scanner;

public class MainForm {
    private final MyPeerServer server;
    private JPanel mainPanel;
    private CanvasPanel canvasPanel;

    public MainForm(MyPeerServer server) {
        this.server = server;
    }

    public static void main(String[] args) {
        MyPeerServer server = new MyPeerServer("localhost", Integer.parseInt(args[0]));
        server.start();
        server.connectToPeer(args[1], Integer.parseInt(args[2]));

        JFrame frame = new JFrame("MainForm");
        frame.setContentPane(new MainForm(server).mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    private void createUIComponents() {
        canvasPanel = new CanvasPanel(server);
    }
}

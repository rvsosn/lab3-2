package ru.rvsosn.lab3;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.rvsosn.lab3.server.AbstractPeerServer;
import ru.rvsosn.lab3.server.Peer;

import java.io.*;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

public class MyPeerServer extends AbstractPeerServer {
    private static Logger logger = LoggerFactory.getLogger(MyPeerServer.class);

    private Set<Line> lines = new HashSet<>();
    private Gson gson = new Gson();
    private Consumer<Line> onNewLine = (Line line) -> { };

    public MyPeerServer(String addr, int port) {
        super(addr, port);
    }

    public void setOnNewLine(Consumer<Line> onNewLine) {
        this.onNewLine = onNewLine;
    }

    public void newLine(Line line) {
        logger.info("Created new line {}", line);
        lines.add(line);
        sendToNextPeer(line);
    }

    @Override
    protected void connectedPeerHandler(Peer peer) throws IOException {
        DataInputStream in = peer.getIn();
        while (true) {
            String data = in.readUTF();

            Line line = gson.fromJson(data, Line.class);

            if (!lines.contains(line)) {
                logger.info("Received data {} from {}", data, peer);

                sendToNextPeer(line);
                onNewLine.accept(line);
                lines.add(line);
            }

        }
    }

    private void sendToNextPeer(Line line) {
        logger.info("Sending {} to next peers", line);
        getToPeers().forEach(peer -> {
            DataOutputStream out = peer.getOut();
            try {
                out.writeUTF(gson.toJson(line));
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
        });
    }

    public Set<Line> getLines() {
        return lines;
    }
}

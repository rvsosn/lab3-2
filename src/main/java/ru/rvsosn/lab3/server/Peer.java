package ru.rvsosn.lab3.server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class Peer {
    private final int id;
    private DataInputStream in;
    private DataOutputStream out;
    private Type type;

    Peer(int id, InputStream in, OutputStream out, Type type) {
        this.id = id;
        this.in = new DataInputStream(in);
        this.out = new DataOutputStream(out);
        this.type = type;
    }

    public DataInputStream getIn() {
        return in;
    }

    public DataOutputStream getOut() {
        return out;
    }

    public Type getType() {
        return type;
    }

    enum Type {
        FROM,
        TO
    }
}
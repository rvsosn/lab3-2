package ru.rvsosn.lab3.server;

import one.util.streamex.StreamEx;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.ConnectException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public abstract class AbstractPeerServer {
    private static Logger logger = LoggerFactory.getLogger(AbstractPeerServer.class);

    private final AtomicInteger peerInc = new AtomicInteger();
    private final List<Peer> peers = new ArrayList<>();
    private final ExecutorService incomingMessagesService = Executors.newCachedThreadPool();
    private final String addr;
    private final int port;

    public AbstractPeerServer(String addr, int port) {
        this.addr = addr;
        this.port = port;
    }

    public void start() {
        Runnable acceptConnectionsHandler = () -> {
            try (ServerSocket serverSocket = new ServerSocket(port)) {
                Socket connected = serverSocket.accept();

                logger.info("Connected peer {}", connected.getInetAddress());

                Peer connectedPeer = newPeer(connected, Peer.Type.FROM);
                peers.add(connectedPeer);

                incomingMessagesService.execute(() -> {
                    logger.debug("Created new incoming message handler for {}", connectedPeer);
                    try {
                        connectedPeerHandler(connectedPeer);
                    } catch (IOException e) {
                        logger.warn("Problem with {}, deleting it", connectedPeer, e);
                        peers.remove(connectedPeer);
                    }
                });
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
        };

        logger.info("Accepting connection on {} {}", addr, port);

        Thread acceptConnectionsThread = new Thread(acceptConnectionsHandler);
        acceptConnectionsThread.setDaemon(false);
        acceptConnectionsThread.start();
    }

    protected abstract void connectedPeerHandler(Peer peer) throws IOException;

    public void connectToPeer(String addr, int port) {
        try {
            while (true) {
                logger.info("Trying connect to peer {} {}", port, addr);
                try {
                    Socket peerSocket = new Socket(addr, port);
                    Peer peer = newPeer(peerSocket, Peer.Type.TO);
                    peers.add(peer);
                    logger.info("Successful connected to peer {}", peer);
                    break;
                } catch (ConnectException ex) { }

                Thread.sleep(1000);
            }

        } catch (IOException e) {
            throw new UncheckedIOException(e);
        } catch (InterruptedException e) {

        }
    }

    protected Stream<Peer> getToPeers() {
        return StreamEx.of(peers)
                .filter(peer -> peer.getType() == Peer.Type.TO);
    }

    private Peer newPeer(Socket socket, Peer.Type type) {
        try {
            return new Peer(peerInc.incrementAndGet(), socket.getInputStream(), socket.getOutputStream(), type);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

}

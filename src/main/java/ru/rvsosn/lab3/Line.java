package ru.rvsosn.lab3;

import java.util.Objects;

/**
 * Линия, используется для передачи и хранения
 */
public class Line {
    private Point from;
    private Point to;

    public Line(Point from, Point to) {
        this.from = from;
        this.to = to;
    }

    @Override
    public String toString() {
        return "Line{" +
                "from=" + from +
                ", to=" + to +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Line line = (Line) o;
        return Objects.equals(from, line.from) &&
                Objects.equals(to, line.to);
    }

    @Override
    public int hashCode() {
        return Objects.hash(from, to);
    }

    public Point getFrom() {
        return from;
    }

    public Point getTo() {
        return to;
    }
}

package ru.rvsosn.lab3;

import java.util.Scanner;

public class Runner {

    public static void main(String[] args) throws InterruptedException {
        MyPeerServer server = new MyPeerServer("localhost", Integer.parseInt(args[0]));

        server.start();

        server.connectToPeer("localhost", Integer.parseInt(args[1]));

        server.setOnNewLine((line -> System.out.println(line)));

        Thread readerThread = new Thread(() -> {
            Scanner in = new Scanner(System.in);
            while (true) {
                server.newLine(new Line(new Point(in.nextInt(), in.nextInt()), new Point(in.nextInt(), in.nextInt())));
            }
        });
        readerThread.start();

    }
}
